# CoppeliaSim ROS Bridge

1. https://github.com/CoppeliaRobotics/simExtROS
2. https://github.com/CoppeliaRobotics/ros_bubble_rob

This repo just for convenient setup.

## System

---

please check you ROS version first:

- For melodic please use the melodic branch it's the default
- For noetic please use the noetic branch `git checkout noetic`
- VREP version tested on the latest version of V4.2.0 rev 5 for Ubuntu 18.04

Package for system:

```bash
sudo apt install xmlschema
```

### Requirement

1. CMake 3.16.0 or higher is required [Carefully reading: HOW TO UPGRADE](https://askubuntu.com/questions/829310/how-to-upgrade-cmake-in-ubuntu)

   ```bash
   cmake --version
   ```

2. Python3 version: 3.8+ [HOW to INSTALL Python 3.8](https://www.itsupportwale.com/blog/how-to-upgrade-to-python-3-8-on-ubuntu-18-04-lts/)

   ```bash
   python3 --version
   ```

   package needed for python3.8:

   ```bash
   pip3 install rospkg xmlschema
   ```

## Build

```bash
cd ~
git clone https://gitlab.com/kinzhang/vrep_ws.git
cd vrep_ws
catkin_make
```

After success building,  you have to add the path of coppelisaim  into .zshrc/ .bashrc as well as the workspace

the ==VREP means the folder have your VREP/coppelisaim==, Some students put VREP at Downloads folders, so you have to modify the first sentence to `echo "export COPPELIASIM_ROOT_DIR=~/Downloads/VREP" >> ~/.bashrc`

```bash
echo "export COPPELIASIM_ROOT_DIR=~/VREP" >> ~/.bashrc
echo "source ~/vrep_ws/devel/setup.bash" >> ~/.bashrc
```

## Run

### 1. roscore

```bash
roscore
```

### 2. VREP/Coppelisaim

the VREP means the folder have your VREP/coppelisaim

```bash
cd ~/VREP
./coppeliaSim.sh
```

and open env.ttt, then click start

### 3. Check topic

```bash
rostopic list
```

![](img/example_0.png)

## More tutorial

Please read and watch tutorial videos from TA on canvas discussion part
